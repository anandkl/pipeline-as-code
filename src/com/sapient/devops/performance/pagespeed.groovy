#!groovy
/*******************************************************************
***** Description :: This Package is used for google pagespeed *****
***** Author      :: Mukul Garg                                *****
***** Date        :: 09/04/2017                                *****
***** Revision    :: 1.0                                       *****
*******************************************************************/

package com.sapient.devops.performance

String SITE_URL

/*************************************
** Function to set the variables.
**************************************/
void siteValue(String site_url)
{
   this.SITE_URL = site_url
}

/*************************************
** Function to test speed for Desktop
**************************************/
def pSpeed_Desktop()
{
   try {
      if ( "${SITE_URL}" == "null" ) {
	    wrap([$class: 'AnsiColorBuildWrapper']) {
          error "\u001B[41m[ERROR] ${SITE_URL} not mentioned..."
        }
	  }
	  else {
        sh returnStatus: true, script: "psi ${SITE_URL} --strategy desktop --format json > pagespeed.json"
      }
   }
   catch (Exception excp) {
      wrap([$class: 'AnsiColorBuildWrapper']) {
         println "\u001B[41m[ERROR] failed to execute..."
		 currentBuild.result = 'FAILED'
		 step([$class: 'StashNotifier'])
         throw excp
      }
   }
}
/*************************************
** Function to test speed for Mobile
**************************************/
def pSpeed_Mobile()
{
   try {
      if ( "${SITE_URL}" == "null" ) {
	    wrap([$class: 'AnsiColorBuildWrapper']) {
          error "\u001B[41m[ERROR] ${SITE_URL} not mentioned..."
        }
	  }
	  else {
        sh returnStatus: true, script: "psi ${SITE_URL} --strategy mobile --format json > pagespeed_mobile.json"
		archiveArtifacts 'pagespeed.json,pagespeed_mobile.json'
      }
   }
   catch (Exception excp) {
      wrap([$class: 'AnsiColorBuildWrapper']) {
         println "\u001B[41m[ERROR] failed to execute..."
		 currentBuild.result = 'FAILED'
		 step([$class: 'StashNotifier'])
         throw excp
      }
   }
}