#!groovy
/****************************************************************
***** Description :: This Package is used for cucumber test *****
***** Author      :: Mukul Garg                             *****
***** Date        :: 09/04/2017                             *****
***** Revision    :: 1.0                                    *****
****************************************************************/

package com.sapient.devops.performance

String REPORTS_PATH

/*************************************
** Function to set the variables.
**************************************/
void setValue(String path)
{
   this.REPORTS_PATH = path
}

/*************************************
** Function to test cucumber
**************************************/
def cucum_Test()
{
   try {
      if ( "${REPORTS_PATH}" == "null" ) {
	    wrap([$class: 'AnsiColorBuildWrapper']) {
          error "\u001B[41m[ERROR] ${REPORTS_PATH} not mentioned..."
        }
	  }
	  else {
	    sh "mvn -e -B clean test"

        cucumber buildStatus: null, fileIncludePattern: '**/*.json', jsonReportDirectory: '${REPORTS_PATH}', sortingMethod: 'ALPHABETICAL'
      }
   }
   catch (Exception excp) {
      wrap([$class: 'AnsiColorBuildWrapper']) {
         println "\u001B[41m[ERROR] failed to execute..."
		 currentBuild.result = 'FAILED'
		 step([$class: 'StashNotifier'])
         throw excp
      }
   }
}