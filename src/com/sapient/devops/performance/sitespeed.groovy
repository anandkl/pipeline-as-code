#!groovy
/***************************************************************
***** Description :: This Package is used for sitespeed.io *****
***** Author      :: Mukul Garg                            *****
***** Date        :: 09/04/2017                            *****
***** Revision    :: 1.0                                   *****
***************************************************************/

package com.sapient.devops.performance

String SITE_URL

/*************************************
** Function to set the variables.
**************************************/
void siteValue(String site_url)
{
   this.SITE_URL = site_url
}

/*************************************
** Function to test speed for Desktop
**************************************/
def speed_Desktop()
{
   try {
      if ( "${SITE_URL}" == "null" ) {
	    wrap([$class: 'AnsiColorBuildWrapper']) {
          error "\u001B[41m[ERROR] ${SITE_URL} not mentioned..."
        }
	  }
	  else {
	    sh "if [ -d 'sitespeed-result' ]; then rm -rf sitespeed-result; fi"
        sh returnStatus: true, script: "sitespeed.io -u ${SITE_URL} --outputFolderName desktop"
		publishHTML([allowMissing: false, alwaysLinkToLastBuild: true, keepAll: false, reportDir: 'sitespeed-result/roche-diagnostics.sapient.com/desktop', reportFiles: 'index.html', reportName: 'Sitespeed Desktop', reportTitles: 'Sitespeed'])
      }
   }
   catch (Exception excp) {
      wrap([$class: 'AnsiColorBuildWrapper']) {
         println "\u001B[41m[ERROR] failed to execute..."
		 currentBuild.result = 'FAILED'
		 step([$class: 'StashNotifier'])
         throw excp
      }
   }
}
/*************************************
** Function to test speed for Mobile
**************************************/
def speed_Mobile()
{
   try {
      if ( "${SITE_URL}" == "null" ) {
	    wrap([$class: 'AnsiColorBuildWrapper']) {
          error "\u001B[41m[ERROR] ${SITE_URL} not mentioned..."
        }
	  }
	  else {
        sh returnStatus: true, script: "sitespeed.io --useragent 'Mozilla/5.0 (Linux; Android 6.0.1; SAMSUNG SM-G925F Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/4.0 Chrome/44.0.2403.133 Mobile Safari/537.36' -u ${SITE_URL} --outputFolderName mobile"
		publishHTML([allowMissing: false, alwaysLinkToLastBuild: true, keepAll: false, reportDir: 'sitespeed-result/roche-diagnostics.sapient.com/mobile', reportFiles: 'index.html', reportName: 'Sitespeed Mobile', reportTitles: 'Sitespeed Mobile'])
      }
   }
   catch (Exception excp) {
      wrap([$class: 'AnsiColorBuildWrapper']) {
         println "\u001B[41m[ERROR] failed to execute..."
		 currentBuild.result = 'FAILED'
		 step([$class: 'StashNotifier'])
         throw excp
      }
   }
}
/*************************************
** Function to test speed for iPad
**************************************/
def speed_iPad()
{
   try {
      if ( "${SITE_URL}" == "null" ) {
	    wrap([$class: 'AnsiColorBuildWrapper']) {
          error "\u001B[41m[ERROR] ${SITE_URL} not mentioned..."
        }
	  }
	  else {
        sh returnStatus: true, script: "sitespeed.io  --useragent 'Mozilla/5.0(iPad; U; CPU iPhone OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B314 Safari/531.21.10' -u ${SITE_URL} --outputFolderName ipad"
		publishHTML([allowMissing: false, alwaysLinkToLastBuild: true, keepAll: false, reportDir: 'sitespeed-result/roche-diagnostics.sapient.com/ipad', reportFiles: 'index.html', reportName: 'Sitespeed iPad', reportTitles: 'Sitespeed iPad'])
      }
   }
   catch (Exception excp) {
      wrap([$class: 'AnsiColorBuildWrapper']) {
         println "\u001B[41m[ERROR] failed to execute..."
		 currentBuild.result = 'FAILED'
		 step([$class: 'StashNotifier'])
         throw excp
      }
   }
}