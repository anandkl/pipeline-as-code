#!groovy
/***************************************************************
***** Description :: This Package is used for ZAP Security *****
***** Author      :: Mukul Garg                            *****
***** Date        :: 08/30/2017                            *****
***** Revision    :: 1.0                                   *****
***************************************************************/

/****
ZAProxy should br installed
Add into customtools and instlla using sample job
****/

package com.sapient.devops.security

String SITE_URL

/*************************************
** Function to set the variables.
**************************************/
void setValue(String site_url)
{
   this.SITE_URL = site_url
}

/*************************************
** Function to compile the code.
**************************************/
def check()
{
   try {
      if ( "${SITE_URL}" == "null" ) {
	    wrap([$class: 'AnsiColorBuildWrapper']) {
          error "\u001B[41m[ERROR] ${SITE_URL} not mentioned..."
        }
	  }
	  else {
		  sh "zap-cli --zap-path ${env.ZAP_HOME} start -o '-config api.key=12345'"
		  sh "zap-cli spider ${SITE_URL}"
		  sh "zap-cli report -f html -o 'zap_result.html'"
		  sh "zap-cli shutdown"
		publishHTML([allowMissing: false, alwaysLinkToLastBuild: true, keepAll: false, reportDir: '.', reportFiles: 'zap_result.html', reportName: 'ZAP Report', reportTitles: 'ZAP'])
		archiveArtifacts 'zap_result.html'
      }
   }
   catch (Exception excp) {
      wrap([$class: 'AnsiColorBuildWrapper']) {
         println "\u001B[41m[ERROR] failed to execute..."
		 currentBuild.result = 'FAILED'
		 step([$class: 'StashNotifier'])
         throw excp
      }
   }
}