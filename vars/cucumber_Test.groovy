#!groovy
/***********************************************************************
***** Description :: This Custom Library is used for Cucumber Test *****
***** Author      :: Mukul Garg                                    *****
***** Date        :: 09/04/2017                                    *****
***** Revision    :: 1.0                                           *****
***********************************************************************/

import com.sapient.devops.performance.cucumber

def call(body) {
  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  try {
	  def cum = new cucumber()
      cum.setValue("${config.REPORTS_PATH}")
      cum.cucum_Test()
  }
  catch (Exception error)
  {
      wrap([$class: 'AnsiColorBuildWrapper']) {
         echo "\u001B[41m[ERROR] ${error}"
         throw error
      }
  }
}