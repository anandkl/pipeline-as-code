#!groovy
/**************************************************************************
***** Description :: This Custom Library is used for google pagespeed *****
***** Author      :: Mukul Garg                                       *****
***** Date        :: 09/04/2017                                       *****
***** Revision    :: 1.0                                              *****
**************************************************************************/

import com.sapient.devops.performance.pagespeed

def call(body) {
  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  try {
	  def pSpeed = new pagespeed()
      pSpeed.siteValue("${config.SITE_URL}")
	  
	  def tasks = [:]
	  
	  tasks["Desktop"] = {
	    stage ("Desktop"){
          node {
            pSpeed.pSpeed_Desktop()
		  }
        }
      }
      tasks["Mobile"] = {
        stage ("Mobile"){    
          node {  
            pSpeed.pSpeed_Mobile()
          }
        }
      }
      parallel tasks
  }
  catch (Exception error)
  {
      wrap([$class: 'AnsiColorBuildWrapper']) {
         echo "\u001B[41m[ERROR] ${error}"
         throw error
      }
  }
}