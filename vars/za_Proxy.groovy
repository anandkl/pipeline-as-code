#!groovy
/**********************************************************************
***** Description :: This Custom Library is used for ZAProxy step *****
***** Author      :: Mukul Garg                                   *****
***** Date        :: 04/24/2017                                   *****
***** Revision    :: 2.0                                          *****
**********************************************************************/

import com.sapient.devops.security.zap

def call(body) {
  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  try {
      def zaProxy = new zap()
      zaProxy.setValue("${config.SITE_URL}")
      zaProxy.check()
  }
  catch (Exception error)
  {
      wrap([$class: 'AnsiColorBuildWrapper']) {
         echo "\u001B[41m[ERROR] ${error}"
         throw error
      }
  }
}
