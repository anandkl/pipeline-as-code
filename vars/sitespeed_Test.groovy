#!groovy
/**********************************************************************
***** Description :: This Custom Library is used for sitespeed.io *****
***** Author      :: Mukul Garg                                   *****
***** Date        :: 09/04/2017                                   *****
***** Revision    :: 1.0                                          *****
**********************************************************************/

import com.sapient.devops.performance.sitespeed

def call(body) {
  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  try {
	  def sSpeed = new sitespeed()
      sSpeed.siteValue("${config.SITE_URL}")
	  
	  def tasks = [:]
	  
	  tasks["Desktop"] = {
	    stage ("Desktop"){
          node {
            sSpeed.speed_Desktop()
		  }
        }
      }
      tasks["Mobile"] = {
        stage ("Mobile"){    
          node {  
            sSpeed.speed_Mobile()
          }
        }
      }
	  tasks["iPad"] = {
        stage ("iPad"){    
          node {  
            sSpeed.speed_iPad()
          }
        }
      }
      parallel tasks
  }
  catch (Exception error)
  {
      wrap([$class: 'AnsiColorBuildWrapper']) {
         echo "\u001B[41m[ERROR] ${error}"
         throw error
      }
  }
}