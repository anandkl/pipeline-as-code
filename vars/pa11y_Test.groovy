#!groovy
/**********************************************************************
***** Description :: This Custom Library is used for Pa11y tests  *****
***** Author      :: Mukul Garg                                   *****
***** Date        :: 09/06/2017                                   *****
***** Revision    :: 2.0                                          *****
**********************************************************************/

import com.sapient.devops.accessibility.pa11y

def call(body) {
  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  try {
      def pAlly = new pa11y()
      pAlly.setValue("${config.SITE_URL}")
      pAlly.pa11yTest()
  }
  catch (Exception error)
  {
      wrap([$class: 'AnsiColorBuildWrapper']) {
         echo "\u001B[41m[ERROR] ${error}"
         throw error
      }
  }
}
